package com.example.s10582.web;

import java.io.Serializable;


import javax.enterprise.context.SessionScoped;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;

import com.example.s10582.domain.Anime;
import com.example.s10582.service.AnimeManager;

@SessionScoped
@Named("animeBean")
public class AnimeFormBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Anime anime = new Anime();

	private ListDataModel<Anime> animes = new ListDataModel<Anime>();

	@Inject
	private AnimeManager an;

	public Anime getAnime() {
		return anime;
	}

	public void setAnime(Anime anime) {
		this.anime = anime;
	}

	public ListDataModel<Anime> getAllAnimes() {
		animes.setWrappedData(an.getAllAnimes());
		return animes;
	}

	
	public String addAnime() {
		an.addAnime(anime);
		return "showAnime";
		
	}

	public String deleteAnime() {
		Anime animeToDelete = animes.getRowData();
		an.deleteAnime(animeToDelete);
		return null;
	}

}
